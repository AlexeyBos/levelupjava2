package org.levelup;

import org.levelup.reflection.ClassFinder;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;

public class HomeWork2 {

    public static void main(String[] args) throws IllegalAccessException, InstantiationException, ClassNotFoundException, NoSuchMethodException, InvocationTargetException {

        Collection<String> classesNames = ClassFinder.searchClassNames("org.levelup.lessons");

        for (String className : classesNames) {
            Object myClassObj = ClassFinder.createClassObj(className);
            System.out.println(myClassObj.toString());
        }

    }
}
