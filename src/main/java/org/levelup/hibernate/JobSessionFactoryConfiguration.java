package org.levelup.hibernate;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

@SuppressWarnings("ALL")
public class JobSessionFactoryConfiguration {

    public SessionFactory configure() {
        Configuration configure = new Configuration().configure();
        SessionFactory factory = configure.buildSessionFactory();
        return factory;
    }

}
