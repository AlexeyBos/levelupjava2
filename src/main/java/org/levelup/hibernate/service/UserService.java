package org.levelup.hibernate.service;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.levelup.application.domain.UserEntity;

public class UserService {

    private final SessionFactory factory;

    public UserService(SessionFactory factory) {
        this.factory = factory;
    }

    public UserEntity createUserPersist(String name, String lastName, String passport) {
        Session session = factory.openSession();
        Transaction transaction = session.beginTransaction();

        UserEntity userEntity = new UserEntity();
        userEntity.setName(name);
        userEntity.setLastName(lastName);
        userEntity.setPassport(passport);

        session.persist(userEntity);
        transaction.commit();
        session.close();

        return userEntity;
    }

    public Integer createUserSave(String name, String lastName, String passport) {
        Session session = factory.openSession();
        Transaction transaction = session.beginTransaction();

        UserEntity userEntity = new UserEntity();
        userEntity.setName(name);
        userEntity.setLastName(lastName);
        userEntity.setPassport(passport);

        Integer generatedId = (Integer) session.save(userEntity);

        transaction.commit();
        session.close();

        return generatedId;
    }

    public UserEntity getById(Integer id) {
        Session session = factory.openSession();
        UserEntity userEntity = session.get(UserEntity.class, id);
        session.close();

        return userEntity;
    }

    public Integer cloneUser(Integer id, String passport) {
        UserEntity userEntity = getById(id);
        Session session = factory.openSession();
        Transaction transaction = session.beginTransaction();

        userEntity.setPassport(passport);
        Integer cloneId = (Integer) session.save(userEntity);

        transaction.commit();
        session.close();

        return cloneId;
    }

    public UserEntity updateUserNameWithMerge(Integer id, String name) {
        UserEntity userEntity = getById(id);
        Session session = factory.openSession();
        Transaction transaction = session.beginTransaction();

        userEntity.setName(name);
        UserEntity mergedUserEntity = (UserEntity) session.merge(userEntity);

        transaction.commit();
        session.close();

        System.out.println("original user: " + Integer.toHexString(userEntity.hashCode()));
        return mergedUserEntity;
    }

    public UserEntity mergeNewUser(String name, String lastName, String passport) {
        Session session = factory.openSession();
        Transaction transaction = session.beginTransaction();

        UserEntity userEntity = new UserEntity();
        userEntity.setName(name);
        userEntity.setLastName(lastName);
        userEntity.setPassport(passport);

        UserEntity newUserEntity = (UserEntity) session.merge(userEntity);

        transaction.commit();
        session.close();

        return newUserEntity;
    }

    public void updateUser(String name, String lastName, String passport) {
        Session session = factory.openSession();
        Transaction transaction = session.beginTransaction();

        UserEntity userEntity = new UserEntity();
        userEntity.setName(name);
        userEntity.setLastName(lastName);
        userEntity.setPassport(passport);

        session.update(userEntity);

        transaction.commit();
        session.close();

    }
}
