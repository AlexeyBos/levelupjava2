package org.levelup.lessons.leveldown;

public class ThirdClass {

    private int oneField;
    private String twoFiled;
    private Long threeField;

    private ThirdClass() {
        this.oneField = 3;
        this.twoFiled = "twoFiled3";
        this.threeField = 33L;
    }

    @Override
    public String toString() {
        return "ThirdClass{" +
                "oneField=" + oneField +
                ", twoFiled='" + twoFiled + '\'' +
                ", threeField=" + threeField +
                '}';
    }
}
