package org.levelup.lessons.leveldown.some.more.down;

import org.levelup.reflection.annotation.RandomInt;

public class ForthClass {

    @RandomInt(min = 5)
    private int oneField;
    private String twoFiled;
    private Long threeField;

    private ForthClass() {
        this.oneField = 4;
        this.twoFiled = "twoFiled4";
        this.threeField = 44L;
    }

    @Override
    public String toString() {
        return "ForthClass{" +
                "oneField=" + oneField +
                ", twoFiled='" + twoFiled + '\'' +
                ", threeField=" + threeField +
                '}';
    }
}
