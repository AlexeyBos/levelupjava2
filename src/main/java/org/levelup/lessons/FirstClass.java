package org.levelup.lessons;

import org.levelup.reflection.annotation.RandomInt;

public class FirstClass {

    @RandomInt(min = 4, max = 20)
    private int oneField;
    private String twoFiled;
    //@RandomInt(min = 1, max = 50)
    private Long threeField;

    private FirstClass() {
        this.oneField = 1;
        this.twoFiled = "value1";
        this.threeField = 11L;
    }

    @Override
    public String toString() {
        return "FirstClass{" +
                "oneField=" + oneField +
                ", twoFiled='" + twoFiled + '\'' +
                ", threeField=" + threeField +
                '}';
    }
}
