package org.levelup.job.list.jdbc;

import org.levelup.job.list.domain.Position;
import org.levelup.job.list.impl.positions.PositionService;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;

public class PositionJdbcService implements PositionService {
    @Override
    public Position createPosition(String name) throws SQLException {
        try (Connection connection = JdbcUtils.getConnection()) {
            //проверим наличие строки с переданным именем
            Position existPosition = findPositionByName(name);
            if (existPosition == null) {
                PreparedStatement statement = connection.prepareStatement("insert into positions (name) values (?)");
                statement.setString(1, name);
                int rowChanged = statement.executeUpdate();
                System.out.println("Количество добавленных строк: " + rowChanged);
                existPosition = findPositionByName(name);
                System.out.println("Должность: ID = " + existPosition.getId() + " name = " + existPosition.getName());
            }
            return existPosition;
        }
    }

    @Override
    public void deletePositionById(int id) throws SQLException {
        try (Connection connection = JdbcUtils.getConnection()) {
            PreparedStatement statament = connection.prepareStatement("delete from positions where id = ?");
            statament.setInt(1, id);

            int rowDeleted = statament.executeUpdate();
            System.out.println("Удалено позиций: " + rowDeleted);
        }
    }

    @Override
    public void deletePositionByName(String name) throws SQLException {
        try (Connection connection = JdbcUtils.getConnection()) {
            PreparedStatement statament = connection.prepareStatement("delete from positions where name = ?");
            statament.setString(1, name);

            int rowDeleted = statament.executeUpdate();
            System.out.println("Удалено позиций: " + rowDeleted);
        }
    }

    @Override
    public Collection<Position> findAllPositionWhichNameLike(String name) throws SQLException {
        try (Connection connection = JdbcUtils.getConnection()) {
            PreparedStatement statement = connection.prepareStatement("select * from positions where name like ?");
            statement.setString(1, name);
            ResultSet resultSet = statement.executeQuery();

            return extractPositions(resultSet);
        }
    }

    private  Collection<Position> extractPositions (ResultSet resultSet) throws SQLException {
        Collection<Position> positions = new ArrayList<>();
        while (resultSet.next()) {
            int id = resultSet.getInt("id");
            String name = resultSet.getString("name");
            positions.add(new Position(id, name));
        }
        return positions;
    }

    @Override
    public Position findPositionById(int id) throws SQLException {
        try (Connection connection = JdbcUtils.getConnection()) {
            Statement selectStatement = connection.createStatement();
            ResultSet resultSet = selectStatement.executeQuery("select * from positions where id = " + id + ""); //for select
            if (resultSet.next()) {
                String positionName = resultSet.getString("name");
                System.out.println("Должность: ID = " + id + " name = " + positionName);
                return new Position(id, positionName);
            } else {
                System.out.println("Должность c идентификатором \"" + id + "\" не найдена");
                return null;
            }
        }
    }

    @Override
    public Collection<Position> findAllPositions() throws SQLException {
        try (Connection connection = JdbcUtils.getConnection()) {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("select * from positions");

            return extractPositions(resultSet);
        }
    }

    @Override
    public Position findPositionByName(String name) throws SQLException {
        try (Connection connection = JdbcUtils.getConnection()) {
            Statement selectStatement = connection.createStatement();
            ResultSet resultSet = selectStatement.executeQuery("select * from positions where name = '" + name + "'"); //for select
            if (resultSet.next()) {
                int id = resultSet.getInt(1);
                String positionName = resultSet.getString("name");
                System.out.println("Должность: ID = " + id + " name = " + positionName);
                return new Position(id, positionName);
            } else {
                System.out.println("Должность c именем \"" + name + "\" не найдена");
                return null;
            }
        }
    }


}
