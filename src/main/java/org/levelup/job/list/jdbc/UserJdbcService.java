package org.levelup.job.list.jdbc;

import org.levelup.job.list.domain.Position;
import org.levelup.job.list.domain.User;
import org.levelup.job.list.impl.users.UserService;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;

public class UserJdbcService implements UserService {


    @Override
    public User createUser(String passport, String name, String lastName) throws SQLException {
        try (Connection connection = JdbcUtils.getConnection()) {
            //проверим наличие строки с переданным паспортом
            User existUser = findByPassport(passport);
            if (existUser == null) {
                PreparedStatement statement = connection.prepareStatement("insert into users (name, last_name, passport) values (?,?,?)");
                statement.setString(1, name);
                statement.setString(2, lastName);
                statement.setString(3, passport);
                int rowChanged = statement.executeUpdate();
                System.out.println("Количество добавленных строк: " + rowChanged);
                existUser = findByPassport(passport);
                System.out.println("User: ID = " + existUser.getId() + " passport = " + existUser.getPassport() +
                        " name = " + existUser.getName() + " last_name = " + existUser.getLast_name());
            } else {
                System.out.println("User with passport \"" + passport + "\" already exists!");
            }
            return existUser;
        }
    }

    @Override
    public User findByPassport(String passport) throws SQLException {
        try (Connection connection = JdbcUtils.getConnection()) {
            Statement selectStatement = connection.createStatement();
            ResultSet resultSet = selectStatement.executeQuery("select * from users where passport = '" + passport + "'");
            if (resultSet.next()) {
                return createUserOjFromCurrentRow(resultSet);
            } else {
                return null;
            }
        }
    }

    @Override
    public Collection<User> findByNameAndLastName(String name, String lastName) throws SQLException {
        try (Connection connection = JdbcUtils.getConnection()) {
            Statement selectStatement = connection.createStatement();
            ResultSet resultSet = selectStatement.executeQuery("select * from  users where name = '" + name + "' and last_name = '" + lastName + "'");
            Collection<User> users = extractUsers(resultSet);
            if (users.size() == 0) {
                System.out.println("Users with name = \"" + name + "\" and last_name = \"" + lastName + "\" doesn't exist");
            }
            return users;
        }
    }

    private User createUserOjFromCurrentRow(ResultSet resultSet) throws SQLException {
        int id = resultSet.getInt("id");
        String name = resultSet.getString("name");
        String lastName = resultSet.getString("last_name");
        String findPassport = resultSet.getString("passport");
        return new User(id, name, lastName, findPassport);
    }

    private  Collection<User> extractUsers(ResultSet resultSet) throws SQLException {
        Collection<User> users = new ArrayList<>();
        while (resultSet.next()) {
            users.add(createUserOjFromCurrentRow(resultSet));
        }
        return users;
    }

    @Override
    public Collection<User> findByLastName(String lastName) throws SQLException {
        try (Connection connection = JdbcUtils.getConnection()) {
            Statement selectStatement = connection.createStatement();
            ResultSet resultSet = selectStatement.executeQuery("select * from  users where last_name = '" + lastName + "'");
            Collection<User> users = extractUsers(resultSet);
            if (users.size() == 0) {
                System.out.println("Users with last_name = \"" + lastName + "\" doesn't exist");
            }
            return users;
        }
    }

    @Override
    public void deleteUserByPassport(String passport) throws SQLException {
        try (Connection connection = JdbcUtils.getConnection()) {
            PreparedStatement statament = connection.prepareStatement("delete from users where passport = ?");
            statament.setString(1, passport);

            int rowDeleted = statament.executeUpdate();
            System.out.println("Удалено пользователей: " + rowDeleted);
        }
    }

    @Override
    public User updateUser(String passport, String name, String lastName) throws SQLException {
        try (Connection connection = JdbcUtils.getConnection()) {
            PreparedStatement statement = connection.prepareStatement("update users set name = ?, last_name = ? where passport = ?");
            statement.setString(1, name);
            statement.setString(2, lastName);
            statement.setString(3, passport);
            return findByPassport(passport);
        }
    }
}
