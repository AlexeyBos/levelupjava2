package org.levelup.job.list.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class JdbcUtils {

    public static Connection getConnection() throws SQLException {
        // url
        // jdbc:<vendor name>://<host(IP address)>/<db name>
        Connection connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/jobs",
                "postgres", "root");
        return connection;
    }

}
