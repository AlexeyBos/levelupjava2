package org.levelup.job.list.hibernate;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.levelup.job.list.domain.Position;
import org.levelup.job.list.impl.positions.PositionService;

import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.Collection;

public class PositionHibernateService implements PositionService {

    private final SessionFactory factory;

    public PositionHibernateService(SessionFactory factory) {
        this.factory = factory;
    }

    @Override
    public Position createPosition(String name) {
        Position position = findPositionByName(name);
        if (position == null) {
            Session session = factory.openSession();
            Transaction transaction = session.beginTransaction();
            position = new Position();
            position.setName(name);
            session.persist(position);
            transaction.commit();
            session.close();
        } else {
            System.out.println("Позиция с именем " + name + " уже существует");
        }

        return position;
    }

    @Override
    public void deletePositionById(int id) {
        Session session = factory.openSession();
        Transaction transaction = session.beginTransaction();
        Position position = session.load(Position.class, id);
        session.delete(position);
        transaction.commit();
        session.close();
    }

    @Override
    public void deletePositionByName(String name) {
        Session session = factory.openSession();
        Transaction transaction = session.beginTransaction();
        Position position = findPositionByName(name);
        session.delete(position);
        transaction.commit();
        session.close();
    }

    @Override
    public Collection<Position> findAllPositionWhichNameLike(String name) {
        Session session = factory.openSession();
        CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
        CriteriaQuery<Position> criteriaQuery = criteriaBuilder.createQuery(Position.class);
        Root<Position> root = criteriaQuery.from(Position.class);

        criteriaQuery.select(root).where(criteriaBuilder.like(root.get("name"), name));
        Query query = session.createQuery(criteriaQuery);
        Collection<Position> positions = query.getResultList();
        session.close();
        return positions;
    }

    @Override
    public Position findPositionById(int id) {
        Session session = factory.openSession();
        Position position = session.get(Position.class, id);
        session.close();

        return position;
    }

    @Override
    public Collection<Position> findAllPositions() {
        Session session = factory.openSession();
        CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
        CriteriaQuery<Position> criteriaQuery = criteriaBuilder.createQuery(Position.class);
        Root<Position> root = criteriaQuery.from(Position.class);

        criteriaQuery.select(root);
        Query query = session.createQuery(criteriaQuery);
        Collection<Position> positions = query.getResultList();
        session.close();
        return positions;
    }

    @Override
    public Position findPositionByName(String name) {
        Session session = factory.openSession();
        CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
        CriteriaQuery<Position> criteriaQuery = criteriaBuilder.createQuery(Position.class);
        Root<Position> root = criteriaQuery.from(Position.class);

        criteriaQuery.select(root).where(criteriaBuilder.equal(root.get("name"), name));
        Query query = session.createQuery(criteriaQuery);
        Collection<Position> positions = query.getResultList();
        session.close();

        Position position = null;
        for (Position rowPosition : positions) {
            position = rowPosition;
            break;
        }
        return position;
    }

}
