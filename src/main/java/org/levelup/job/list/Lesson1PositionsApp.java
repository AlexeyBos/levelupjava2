package org.levelup.job.list;

import org.levelup.job.list.domain.Position;
import org.levelup.job.list.jdbc.PositionJdbcService;

import java.sql.SQLException;
import java.util.Collection;

public class Lesson1PositionsApp {

    public static void main(String[] arg) throws SQLException {
        PositionJdbcService positionService = new PositionJdbcService();

        System.out.println("Execute createPosition(\"Manager\")");
        Position newPosition = positionService.createPosition("Manager");

        System.out.println("Execute findPositionByName(\"Manager\")");
        Position position = positionService.findPositionByName("Manager");

        System.out.println("Execute deletePositionByName(\"Manager\")");
        positionService.deletePositionByName("Manager");

        System.out.println("Execute findPositionByName(\"Manager\")");
        position = positionService.findPositionByName("Manager");

        System.out.println("Execute createPosition(\"CEO\")");
        newPosition = positionService.createPosition("CEO");
        System.out.println("Execute findPositionById(newPosition.getId()");
        position = positionService.findPositionById(newPosition.getId());

        System.out.println("Execute deletePositionById(position.getId()");
        positionService.deletePositionById(position.getId());

        System.out.println("Execute findAllPositionWhichNameLike(\"%evelop%\")");
        Collection<Position> likeRows = positionService.findAllPositionWhichNameLike("%evelop%");
        for (Position row : likeRows) {
            System.out.println(row.getId() + "  " + row.getName());
        }

        System.out.println("Execute findAllPositionWhichNameLike(\"%evelop%\")");
        Collection<Position> allRows = positionService.findAllPositions();
        for (Position row : allRows) {
            System.out.println(row.getId() + "  " + row.getName());
        }

    }

}
