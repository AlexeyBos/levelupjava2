package org.levelup.job.list;

import org.levelup.job.list.domain.User;
import org.levelup.job.list.jdbc.UserJdbcService;

import java.sql.SQLException;
import java.util.Collection;

public class Lesson1UsersApp {

    public static void main(String[] arg) throws SQLException {
        UserJdbcService userService = new UserJdbcService();

        System.out.println("Execute createUser(\"1234 123456\", \"Leo\", \"King\")");
        User newUser = userService.createUser("1234 123456", "Leo", "King");
        System.out.println("Execute the same createUser");
        User newUser2 = userService.createUser("1234 123456", "Leo", "King");
        System.out.println("Execute deleteUserByPassport(\"1234 123456\")");
        userService.deleteUserByPassport("1234 123456");

        System.out.println("Execute creates for find methods");
        newUser = userService.createUser("1234 123456", "Leo", "King");
        newUser = userService.createUser("5678 123456", "Leo", "King");
        newUser = userService.createUser("9012 123456", "Bobby", "King");
        System.out.println("Execute findByLastName(\"King\")");
        Collection<User> users = userService.findByLastName("King");
        users.forEach(user -> {
            System.out.println("User with ID = " + user.getId() + " name = \"" + user.getName() + "\" last_name = \""
                    + user.getLast_name() + "\" passport = \"" + user.getPassport() + "\"");
        });
        System.out.println("Execute findByNameAndLastName(\"Leo\", \"King\")");
        users = userService.findByNameAndLastName("Leo", "King");
        users.forEach(user -> {
            System.out.println("User with ID = " + user.getId() + " name = \"" + user.getName() + "\" last_name = \""
                    + user.getLast_name() + "\" passport = \"" + user.getPassport() + "\"");
        });

        System.out.println("Execute findByLastName(\"Leo\")");
        users = userService.findByLastName("Leo");
        System.out.println("Execute findByNameAndLastName(\"Leo1\", \"King\")");
        users = userService.findByNameAndLastName("Leo1", "King");

        User updUser = userService.updateUser("9012 123456", "Leon", "Killer");
        System.out.println("After update user with passport = \"" + updUser.getPassport() + "\" has name = \"" +
                updUser.getName() + "\" last_name = \"" + updUser.getLast_name() + "\"");
    }

}
