package org.levelup.job.list.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "users")
public class User {

    @Id //primary key
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(length = 100, nullable = false)
    private String name;
    @Column(name = "last_name", length = 100, nullable = false)
    private String last_name;
    @Column(length = 50, nullable = false, unique = true)
    private String passport;

    public User() {}

    public User(int id, String name, String lastName, String passport) {
        this.id = id;
        this.name = name;
        this.last_name = lastName;
        this.passport= passport;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getLast_name() {
        return last_name;
    }

    public String getPassport() {
        return passport;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public void setPassport(String passport) {
        this.passport = passport;
    }
}
