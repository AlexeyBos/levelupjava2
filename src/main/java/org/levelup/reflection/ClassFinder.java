package org.levelup.reflection;

import java.io.File;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;

public class ClassFinder {

    private static final String srcPath = "src\\main\\java\\";
    private static final String ext = ".java";

    public static Collection<String> searchClassNames(String packName) {
        String initialPackPath = srcPath + packName.replace(".", "\\");
        return getClassNames(initialPackPath);
    }

    private static Collection<String> getClassNames(String initialPackPath) {
        Collection<String> classes = new ArrayList<>();
        File[] files = new File(initialPackPath).listFiles();
        if (files != null) {
            for (File file : files) {
                String fileName = file.getName();
                if (fileName.endsWith(ext)) {
                    String className = makeClassName(initialPackPath, fileName);
                    classes.add(className);
                } else if (file.isDirectory()) {
                    Collection<String> classesFromNextDir = getClassNames(file.getPath());
                    classes.addAll(classesFromNextDir);
                }
            }
        }
        return classes;
    }

    private static String makeClassName(String filePath, String fileName) {
        String className = filePath.replace(srcPath, "");
        className += "\\" + fileName.replace(ext, "");
        return className.replace("\\", ".");
    }

    public static Object createClassObj(String className) throws ClassNotFoundException, IllegalAccessException, InstantiationException, NoSuchMethodException, InvocationTargetException {

        Class<?> myClass = Class.forName(className);
        Constructor<?> myClassDeclaredConstructor = myClass.getDeclaredConstructor();
        myClassDeclaredConstructor.setAccessible(true);
        Object myClassObj = myClassDeclaredConstructor.newInstance();

        return myClassObj;
    }

}
