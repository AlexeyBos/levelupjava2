package org.levelup.reflection.annotation;

import java.lang.reflect.Field;
import java.util.Random;

public class RandomIntAnnotationProcessor {

    public static Object process(Object object) throws IllegalAccessException {
        Class<?> objClass = object.getClass();

        Field[] fields = objClass.getDeclaredFields();
        for (Field field : fields) {
            RandomInt annotation = field.getDeclaredAnnotation(RandomInt.class);
            if (annotation != null) {
                if (field.getType() != int.class) {
                    throw new RuntimeException("Аннотация RandomInt может использоваться только с полями типа int. Поле " +
                            field.getName() + " имеет тип " + field.getType());
                }
                int min = annotation.min();
                int max = annotation.max();
                Random random = new Random();
                int randomValue = min + random.nextInt(max - min);
                field.setAccessible(true);
                field.setInt(object, randomValue);
            }
        }
        return object;
    }

}
