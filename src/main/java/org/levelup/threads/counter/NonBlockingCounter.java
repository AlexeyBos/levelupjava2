package org.levelup.threads.counter;

import java.util.concurrent.atomic.AtomicInteger;

//CAS - compare and swap
public class NonBlockingCounter extends Counter {

    private AtomicInteger atomicInteger = new AtomicInteger(0);

    @Override
    public void incrementCounter() {
        atomicInteger.incrementAndGet();
    }

    @Override
    public synchronized int getCounter() {
        return atomicInteger.get();
    }
}
