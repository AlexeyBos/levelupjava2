package org.levelup.threads.counter;

public class Counter {

    protected int counter;

    //критическая секция - блок кода, который выполняется только одним потоком в один момент времени
    public void incrementCounter() {
        synchronized (this) {
            counter++;
        }
    }

    public synchronized int getCounter() {
        return counter;
    }
}
