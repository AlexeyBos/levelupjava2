package org.levelup.threads;

import lombok.SneakyThrows;

@SuppressWarnings("ALL")
public class ThreadExample {

    @SneakyThrows
    public static void main(String[] args) {
        Thread codeAnalyzer = new Thread(new BackgroundTask());
        codeAnalyzer.setDaemon(true);
        codeAnalyzer.start();
        //extends Thread
        //implements Runnable
        //implements Callable

        Thread helloWorldThread = new Thread(new HelloWorldRunnable(), "Hello-World-Thread");
        System.out.println("State before start " + helloWorldThread.getState());
        helloWorldThread.start();

        CounterThread counter = new CounterThread();
        counter.start();
        Thread counter2 = new CounterThread();
        counter2.start();

        System.out.println("Main thread");
        String threadName = Thread.currentThread().getName();
        System.out.println("Thread name:" + threadName);

        Thread.sleep(12000);
        System.out.println("Main thread finished");
    }

    //Thread states
    // NEW - создан, но не запущен (start())
    // RUNNABLE - поток запущен и готов выполняться (т.е. готов получить ресурсы для выполнения на ядре)
    // RUNNING - поток запущен и выполняется на ядре
    // BLOCKED/WAITING - Поток прерван, поток ожидает какой-либо ресурс
    // WAITING - Поток перван, поток ждет действия, которое его разбудит
    // TIMED WAITING - поток прерван, поток ждет действия, которое его разбудит. Но ждет ограниченное время.
    // TERMINATED - Поток завершен

    static class CounterThread extends Thread {

        @SneakyThrows
        @Override
        public void run() {
            String threadName = Thread.currentThread().getName();
            System.out.println("Thread name:" + threadName);
            for (int i = 0; i < 10; i++) {
                Thread.sleep(1000);
                System.out.println(threadName + ": " + i);
            }
        } // когда заканчивается run, поток завершается
    }

    static class HelloWorldRunnable implements Runnable {

        @SneakyThrows
        @Override
        public void run() {
            Thread.sleep(4000);
            System.out.println("Hello from " + Thread.currentThread().getName());
        }

    }

    static class BackgroundTask implements Runnable {

        @SneakyThrows
        @Override
        public void run() {
            while (true) {
                Thread.sleep(500);
                System.out.println("Code analyzer started...");
            }
        }
    }

}
