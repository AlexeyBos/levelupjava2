package org.levelup.application;

import org.hibernate.SessionFactory;
import org.levelup.application.dao.CompanyDao;
import org.levelup.application.dao.CompanyDaoImpl;
import org.levelup.application.dao.CompanyLegalDetailsDao;
import org.levelup.application.dao.CompanyLegalDetailsDaoImpl;
import org.levelup.application.dao.JobListDao;
import org.levelup.application.dao.JobListDaoImpl;
import org.levelup.application.dao.PositionDao;
import org.levelup.application.dao.PositionDaoImpl;
import org.levelup.application.dao.UserDao;
import org.levelup.application.dao.UserDaoImpl;
import org.levelup.application.domain.JobListEntity;
import org.levelup.application.domain.PositionEntity;
import org.levelup.application.domain.UserAddressEntity;
import org.levelup.hibernate.JobSessionFactoryConfiguration;
import org.levelup.application.domain.UserEntity;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;

public class JobApplication {

    public static void main(String[] args) {

        SessionFactory factory = new JobSessionFactoryConfiguration().configure();
        CompanyDao companyDao = new CompanyDaoImpl(factory);
        CompanyLegalDetailsDao companyLegalDetailsDao = new CompanyLegalDetailsDaoImpl(factory);
        companyDao.create("Company jobList", "7565784611", "Spb");
        Integer companyId = companyDao.findByEin("7565784611").getId();
        /*CompanyEntity company = companyDao.findByEin("685-6695567");
        companyLegalDetailsDao.updateLegalDetailsInCompany(company.getId(), "Sberbank", "3534546474");

        Collection<CompanyLegalDetailsEntity> legalDetails = companyLegalDetailsDao.findAllByBankName("Sberbank");
        legalDetails.forEach(legalDetail -> System.out.println(legalDetail.getCompany().getName()));*/

        UserDao userDao = new UserDaoImpl(factory);
        UserEntity user = userDao.createUser("Alex1", "Alexeev1", "1111 999999", new ArrayList<>(Arrays.asList(
                "address 1",
                "address 2",
                "address 3"
        )));
        Integer userId = user.getId();

        PositionDao positionDao = new PositionDaoImpl(factory);
        Integer positionId = positionDao.createPosition("Product Owner").getId();

        JobListDao jobListDao = new JobListDaoImpl(factory);
        jobListDao.createJobRecord(companyId, userId, positionId, LocalDate.of(2019, 12, 4), null);

        JobListEntity jobListRecord = jobListDao.findJobRecord(companyId, userId, positionId);
        System.out.println(jobListRecord.getCompany());
        System.out.println(jobListRecord.getPosition());
        System.out.println(jobListRecord.getUser());
        System.out.println(jobListRecord.getStartDate());
        System.out.println(jobListRecord.getEndDate());

        /*for (UserAddressEntity addressEntity : user.getAddresses()) {
           System.out.println(addressEntity.getId() + " " + addressEntity.getAddress());
        }*/

        /*companyDao.findById(1);

        CompanyEntity company = companyDao.findByEin("12346456");
        System.out.println(company);

        companyDao.create("Apple", "1029384756", "Cupertino");*/



        factory.close();

    }

}
