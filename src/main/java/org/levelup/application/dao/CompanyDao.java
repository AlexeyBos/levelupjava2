package org.levelup.application.dao;

// DAO - Data Access Object

//Controller DTO (Data Transfer Object)
//Services DTO
//DAO Entity
//DB

import org.levelup.application.domain.CompanyEntity;

public interface CompanyDao {

    //CRUD - CreateReadUpdateDelete
    // create, findAll, findById, update, delete

    void create(String name, String ein, String address);

    CompanyEntity findById(Integer id);

    CompanyEntity findByEin(String ein);

    CompanyEntity findByName(String name);

}
