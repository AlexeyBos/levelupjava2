package org.levelup.streams;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class StreamsExample {

    public static void main(String[] args) {
        Collection<Integer> integers = new ArrayList<>();
        integers.add(50);
        integers.add(34);
        integers.add(53);
        integers.add(23);
        integers.add(3834);
        integers.add(44);
        integers.add(74);
        integers.add(2156);
        integers.add(97);
        integers.add(67);
        integers.add(64);

        List<Integer> sortedCollection = new ArrayList<>(integers);
        Collections.sort(sortedCollection);

        List<String> integersAsString = new ArrayList<>(integers.size());
        for (Integer integer : integers) {
            integersAsString.add(String.valueOf(integer));
        }

        Collection<Integer> sorted = integers.stream().sorted().collect(Collectors.toList());

        Collection<String> strings = integers
                .stream()
                .filter(integer -> integer > 9 && integer < 100)
                //Method reference
                //.map(integer -> String.valueOf(integer))
                .map(String::valueOf) // это тоже самое, что и выше строчкой - Method reference
                .filter(string -> string.length() <= 3)
                .collect(Collectors.toList());

        Boolean al = strings.stream().allMatch(string -> string.length() == 2);

    }

}
