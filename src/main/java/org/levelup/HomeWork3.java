package org.levelup;

import org.levelup.reflection.ClassFinder;
import org.levelup.reflection.annotation.RandomIntAnnotationProcessor;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;

public class HomeWork3 {

    public static void main(String[] args) throws IllegalAccessException, InstantiationException, ClassNotFoundException,
            NoSuchMethodException, InvocationTargetException {

        Collection<String> classesNames = ClassFinder.searchClassNames("org.levelup.lessons");

        for (String className : classesNames) {
            Object myClassObj = ClassFinder.createClassObj(className);
            System.out.println(myClassObj.toString());
            System.out.println("After process annotation");
            Object myClassObjAfterAnnotation = RandomIntAnnotationProcessor.process(myClassObj);
            System.out.println(myClassObjAfterAnnotation.toString());
            System.out.println("------------------------------");
        }

    }
}
