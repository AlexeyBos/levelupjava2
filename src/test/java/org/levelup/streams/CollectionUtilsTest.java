package org.levelup.streams;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

class CollectionUtilsTest {

    // test<method name>_when<Input parameters>_then<Result>
    //testRemoveLongStrings_whenCollectionIsValid_thenFilterCollection
    //testRemoveLongStrings_CollectionIsValid_FilterCollection
    @Test
    public void testRemoveLongStrings_whenCollectionIsEmpty_thenReturnEmptyCollection() {
        Collection<String> emptyCollection = new ArrayList<>();
        Collection<String> result = CollectionUtils.removeLongStrings(emptyCollection, 10);
        Assertions.assertTrue(result.isEmpty());
        Assertions.assertNotSame(emptyCollection, result);
    }

    @Test
    public void testRemoveLongStrings_whenCollectionIsNull_throwsExeption() {
        Assertions.assertThrows(NullPointerException.class, () -> CollectionUtils.removeLongStrings(null, 10));
    }

    @Test
    public void testRemoveLongStrings_whenCollectionIsValid_returnedFilteredCollection() {
        Collection<String> originalCollection = new ArrayList<>(Arrays.asList("String1", "Long String1"));
        Collection<String> result = CollectionUtils.removeLongStrings(originalCollection, 10);
        Assertions.assertNotEquals(originalCollection.size(), result.size());
        Assertions.assertEquals(1, result.size());

        boolean isAllStringLengthsLessThen10 = result.stream()
                .allMatch(string -> string.length() < 10);
        Assertions.assertTrue(isAllStringLengthsLessThen10);
    }
}