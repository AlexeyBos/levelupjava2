package org.levelup.application.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.levelup.application.domain.PositionEntity;
import org.levelup.configuration.HibernateTestConfiguration;

import javax.persistence.PersistenceException;

import static org.junit.jupiter.api.Assertions.*;

public class PositionDaoImplIntegrationTest {

    private static SessionFactory factory;
    private static PositionDao positionDao;

    @BeforeAll
    public static void setupPositionDao() {
        factory = HibernateTestConfiguration.getFactory();
        positionDao = new PositionDaoImpl(factory);
    }

    @Test
    @DisplayName("Create new position, then position with this name doesn't exist")
    public void testCreatePosition_whenPositionWithNameNotExists_thenCreateNewPosition() {
        String name = "Java Developer";
        PositionEntity result = positionDao.createPosition(name);
        assertNotNull(result.getId());
        assertEquals(name, result.getName());

        Session session = factory.openSession();
        PositionEntity entityFromDB = session.createQuery("from PositionEntity where name = :name", PositionEntity.class)
                .setParameter("name", name)
                .getSingleResult();
        assertNotNull(entityFromDB);
        session.close();
    }

    @Test
    @DisplayName("Throw exception when position name exists")
    public void testCreatePosition_whenPositionExists_thenThrowException() {
        String name = "PositionName";
        positionDao.createPosition(name);
        assertThrows(PersistenceException.class, () -> positionDao.createPosition(name));
    }

}
