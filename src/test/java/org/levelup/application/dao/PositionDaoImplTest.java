package org.levelup.application.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.levelup.application.domain.PositionEntity;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

class PositionDaoImplTest {

    @Mock
    private SessionFactory factory;
    @Mock
    private Session session;
    @Mock
    private Transaction transaction;

    //private PositionDao dao;
    // а можно так и мокито
    // сам вызовет конструктор dao = new PositionDaoImpl(factory);
    @InjectMocks
    private PositionDaoImpl dao;


    @BeforeAll
    public static void BeforeAll() {
        System.out.println("BeforeAll");
    }

    @BeforeEach
    public void initialize() {
        MockitoAnnotations.initMocks(this);
        /*factory = mock(SessionFactory.class);
        session = mock(Session.class);
        transaction = mock(Transaction.class);
        */
        when(factory.openSession()).thenReturn(session);
        when(session.beginTransaction()).thenReturn(transaction);

        //dao = new PositionDaoImpl(factory);
    }

    @Test
    public void testCreatePosition_validParams_persistNamePosition() {
        String name = "position name";

        PositionEntity entity = dao.createPosition(name);
        assertEquals(name, entity.getName());
        verify(session).persist(ArgumentMatchers.any(PositionEntity.class));
        Mockito.verify(transaction).commit();
        Mockito.verify(transaction, Mockito.times(1)).commit();
        Mockito.verify(session).close();
    }

    @AfterEach
    public void afterEach() {
        System.out.println("afterEach");
    }

    @AfterAll
    public static void afterAll() {
        System.out.println("afterAll");
    }

}